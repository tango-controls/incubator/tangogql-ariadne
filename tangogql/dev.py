"""
Development server entrypoint. Useful because it includes a GraphQL Playground
application.
"""

from ariadne.asgi import GraphQL
from ariadne.asgi.handlers import GraphQLTransportWSHandler

from .common import get_context_value
from .schema import schema

app = GraphQL(
    schema,
    debug=True,
    context_value=get_context_value,
    websocket_handler=GraphQLTransportWSHandler(),
    # logger="ariadne2",
)
